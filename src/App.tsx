import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import Home from "./pages/home";
import Heder from "./pages/header";
import Landing from "./pages/landing";
import Pokemon from "./pages/pokemon";
import './app.css';
// function App(Router:typeof React.Component){
    
function App() {
  return (
    <Router>
      <Heder></Heder>
      <Routes>
        <Route path="/home" element={<Home />}></Route>
        <Route path="/" element={<Landing />}></Route>
        <Route path="/pokemon/:name" element={<Pokemon/>}></Route>
      </Routes>
    </Router>
  );
}

export default App;
