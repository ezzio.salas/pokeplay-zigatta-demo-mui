import styled from 'styled-components';

export const Header = styled.header`
    display: flex;      
    justify-content: space-between;
    align-items: center;
    position:sticky;
    top:0;
    background-color: #fff;
    z-index:100;
    width: 100%;
    
`

export const Header_icon =styled.img`
    object-fit:contain;
    height: 40px;
    marging-left: 20px;`

export const Header__center = styled.div`
    display: flex;
    flex:1;
    align-items: center;
    max-width: fit-content;
    padding: 10px;
    height: 30px;
    border:1px solid lightgray;
    border-radius: 99px;
    >input{
        border:none;
        width: 250px;
        outline-width:0;
    }
    `

export const Header_right = styled.div`
    display: flex;
    align-items: center;
    justuyfy-content: space-between;
    width: 15vw;
    margin-right: 80px;
    `    