import { height } from "@mui/system";
import styled  from "styled-components";

export const Baner = styled.div`
    background:url("https://p4.wallpaperbetter.com/wallpaper/957/704/964/pokemon-fields-ruby-1920x1200-nature-fields-hd-art-wallpaper-preview.jpg");    
    height:50vh;
    margin-bottom:20px !important;
    position: relative;`

 export const Baner__info=styled.div`
    background-color:black;
    color:white;
    padding-top:10vh;
    padding-left:50px;
    padding-right:50px;
    padding-bottom:40px;
    width:200px;
    >button{
        background-color:#ffb300;
        color:#004ba0;
        text-transform:inherit;
        margin-top:20px;
        font-weight:600;
    };
    >h5{
        margin-top:20px;
    }
    >button:hover{
        background-color:#004ba0;
        color:#ffb300;
    }
 `   

 export const Baner__search=styled.div` 
    display:flex;
    flex-direction:column;
 `

 export const Button__search=styled.div`
    >button{background-color: white;
    font-weight: 900;
    text-transform: inherit;
    color:black;
}
>button:hover{
    background-color: #004ba0;
    color:white;
}
    `
