import  {URL} from '../utils/TYPES';

export async function getData<TypeURL>(params:URL):Promise<TypeURL>{
  return fetch(`${params.host}${params.route}${params.id}`).then(response=>{
    if(!response.ok){
        throw new Error(response.statusText);
    }
    else {
        return response.json() as Promise<TypeURL>;
    }
    
})}


