import { getData } from "./service"; 
import { URL,POKEMON } from "../utils/TYPES";

export function ApiGetPokemon(pokemon:string){
    const params:URL = {
        host:process.env.URL_HOST_POKEURL!,
        route: "pokemon/",
        id: pokemon
    }
    return getData<POKEMON>(params);
}

