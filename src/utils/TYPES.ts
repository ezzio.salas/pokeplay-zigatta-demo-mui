export  interface URL{
    host:string;
    route:string
    id:string;
}

export interface POKEMON {
    name:string;
    order:number;
    sprites:{back_default:string};
    types:[];
}