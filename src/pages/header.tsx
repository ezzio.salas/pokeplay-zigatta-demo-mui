import React from 'react'
import {Header,Header__center,Header_icon,Header_right} from '../components/header'
import SearchIcon from '@mui/icons-material/Search';
import TrackChanges from '@mui/icons-material/TrackChanges';
export default function header() {
  return (<>
  
    <Header>
    <Header_icon src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1920px-International_Pok%C3%A9mon_logo.svg.png"></Header_icon>    
    <Header__center>
        <input type="text" placeholder="Search"></input>
        <SearchIcon></SearchIcon>                
        
    </Header__center>    
    <Header_right>Become a Trainer
        <TrackChanges></TrackChanges>
    </Header_right>
    </Header>

    </>
  )
}
