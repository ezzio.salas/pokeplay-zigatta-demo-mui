import React from 'react'
import { Baner,Baner__info } from "../components/baner";
import { Button } from "@mui/material";
export default function home() {
  return (
    <Baner>
    <h1>Im a banner</h1>
    <Baner__info>
        <h1>Get out and catch them all.</h1>
        <h5>Over 150 to get</h5>
        <Button variant="outlined">Catch</Button>
    </Baner__info>
</Baner>
  )
}
