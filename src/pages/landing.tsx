import React from 'react'
import { Baner,Baner__info, Baner__search,Button__search} from "../components/baner";
import { Button } from "@mui/material";
import '../styles/Baner.css';
import '../styles/Landing.css'
export default function landing() { 
return (
     <Baner>
    <div className='baner__search'> 
     <Button className='button__search' variant="outlined">Catch</Button>
    </div>
  <Baner__info>
      <h1>Get out and catch them all.</h1>
      <h5>Over 150 to get</h5>
      <Button variant="outlined">Catch</Button>
  </Baner__info>
</Baner>
 
  )
}
