import React, { useEffect } from "react";
import { ApiGetPokemon } from "../services/api";
import { POKEMON } from "../utils/TYPES";
import {useParams} from 'react-router-dom'
export default function Pokemon() {
  const [pokemon, setPokemon] = React.useState<POKEMON>({
    name: "",
    order: 0,
    sprites: { back_default: "" },
    types: [],
  });
  const handle= useParams();

   useEffect(() => {
    getPokemon();
    async function getPokemon() {
      const res = await ApiGetPokemon(handle.name!);
      const pokemon: POKEMON = {
        name: res.name,
        order: res.order,
        sprites: res.sprites,
        types: res.types,
      };
      setPokemon(pokemon);
    }
  }, []);



  
  return (
    <>
  
      <div>{pokemon.name}</div>

      <div>{pokemon.order}</div>

      <div>{pokemon.types.map((i: any) => i.type.name)}</div>

      <img src={pokemon.sprites.back_default}></img>
    </>
  );
}
